import moment from "moment";

export function formatMessage(
  nickname: string,
  username: string,
  text: string,
  profilepic: string
) {
  return {
    nickname,
    username,
    text,
    time: moment().format("h:mm a"),
    profilepic
  };
}

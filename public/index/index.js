const buttons = document.querySelectorAll(".button");
for (button of buttons) {
  button.addEventListener("click", function (event) {
    let direction = event.currentTarget.getAttribute("direction");
    switch (direction) {
      case "notice":
        window.location = "../notice/notice.html";
        break;
      case "mission":
        window.location = "../route_menu/route_menu.html";
        break;
      case "forum":
        window.location = "../mainforum/forumcat.html";
        break;
      case "shop":
        window.location = "../shop/shop.html";
        break;
      case "profile":
        window.location = "../profile/profile.html";
        break;
    }
  });
}

document.querySelector(".guild").addEventListener("click", async function (event) {
  const res = await fetch("/index");
  const result = await res.json();
  if(result.guild_id){
    window.location = "../guild/guild_info.html";
  }else{
    window.location = "../guild/guild.html";
  }
  
});

document.querySelector(".getName").addEventListener("click", function (event) {
  window.location = "../profile/profile.html";
});

async function loadUserInfo() {
  const nameContainer = document.querySelector(".getName");
  const res = await fetch("/index");
  const result = await res.json();
  if (res.status === 200) {
    data = result;
    nameContainer.innerHTML = `<h1>${data.nickname}</h1>`;
  }
}

loadUserInfo();

function expand() {
  document.querySelector(".mission").style.left = "32%";
  document.querySelector(".mission").style.top = "27%";
  document.querySelector(".guild").style.right = "32%";
  document.querySelector(".guild").style.top = "27%";
  document.querySelector(".forum").style.bottom = "10%";
  document.querySelector(".forum").style.left = "50%";
  document.querySelector(".profile").style.bottom = "10%";
  document.querySelector(".profile").style.right = "50%";
  document.querySelector(".shop").style.bottom = "35%";
  document.querySelector(".collapse").style["z-index"] = "12";
  const ops = document.querySelectorAll(".inner-text");
  for (let op of ops) {
    op.style.transition = "0.3s ease";
    op.style["transition-delay"] = "0.5s";
    op.style.opacity = "1";
  }
}

function collapse() {
  document.querySelector(".mission").style.left = "0";
  document.querySelector(".mission").style.top = "0";
  document.querySelector(".guild").style.right = "0";
  document.querySelector(".guild").style.top = "0";
  document.querySelector(".forum").style.bottom = "0";
  document.querySelector(".forum").style.left = "0";
  document.querySelector(".profile").style.bottom = "0";
  document.querySelector(".profile").style.right = "0";
  document.querySelector(".shop").style.bottom = "0";
  document.querySelector(".collapse").style["z-index"] = "-2";
  const ops = document.querySelectorAll(".inner-text");
  for (let op of ops) {
    op.style.transition = "0.3s ease";
    op.style["transition-delay"] = "0.5s";
    op.style.opacity = "0";
  }
}

document.querySelector("#logout").addEventListener("click", async () => {
  const res = await fetch("/logout");
  const result = await res.json();
  if (res.status === 200) {
    data = result;
    console.log(data);
    window.location = "../login/login.html";
  }
});

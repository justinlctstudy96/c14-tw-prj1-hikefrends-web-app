// let map; ///for google map


// const { json } = require("express")

const socket = io()

// function getRouteID(){
//     var url = document.href
//     let params = url.split('?')[1].split('&')
//     // let routeID = param[0].split('=')[1]
//     return params
// }

// var i = getRouteID()
var userID = 0;
var username = ""

var routeID = (document.location.href).split('?')[1].split('=')[1]
let guild = false;
let form_exist = false;
var activity_center_coordinate = {}
document.querySelector(".back").addEventListener('click', async function () {
    const res = await fetch("/index");
    if (res.status == 200) {
        window.history.back();
    }
})
document.querySelector(".today_weather").addEventListener('click', function () {
    document.querySelector(".today_weather_details").classList.toggle('display_none')
})



async function loadRoute() {
    desExpand()
    createActivityButtonAction()
    const res = await fetch(`/routeinfo/${routeID}`);
    const route = await res.json()
    let routeCoordinates = []
    routeJson = route.rows[0].coordinates
    for (var j in routeJson) {
        routeCoordinates.push([routeJson[j].longitude, routeJson[j].latitude])
    }
    loadMapBox(routeCoordinates)

    let directions = document.querySelectorAll(".direction")
    for (direction of directions) {
        direction.addEventListener('click', function (event) {
            if (event.currentTarget.innerText == "Start") {
                drawDirection(routeCoordinates, routeCoordinates[0]) //start of hiking route
                event.currentTarget.classList.add('direction_click')
                try{document.querySelector('.end').classList.remove('direction_click')}catch(e){}
            } else { //end of hiking route
                drawDirection(routeCoordinates, routeCoordinates[routeCoordinates.length - 1])
                event.currentTarget.classList.add('direction_click')
                try{document.querySelector('.start').classList.remove('direction_click')}catch(e){}
            }
        })
    }

    document.querySelector(".right").addEventListener('click', function (event) {
        event.currentTarget.classList.add('click')
        document.querySelector(".left").classList.remove('click')
        document.querySelector(".mapOrPhoto").innerHTML = `<div class='scene' style="background-image: url('../route_pic/${route.rows[0].name}.jpg');background-size:auto 100%;"></div>`
    })

    document.querySelector(".left").addEventListener('click', function (event) {
        event.currentTarget.classList.add('click')
        document.querySelector(".right").classList.remove('click')
        document.querySelector(".mapOrPhoto").innerHTML = `<div id='map' style='width: 100%; height: 100%;'><div class="directions">
                                                                <i class="fas fa-directions"></i>
                                                                <div class="direction start">Start</div>
                                                                <div class="direction end">End</div>
                                                              </div>
                                                            </div>`
        loadMapBox(routeCoordinates)
        let directions = document.querySelectorAll(".direction")
        for (direction of directions) {
            direction.addEventListener('click', function (event) {
                // let location =  getLocation()
                // let currentLocation = [location.coords['latitude'],location.coords['longitude']]
                if (event.currentTarget.innerText == "Start") {
                    drawDirection(routeCoordinates, routeCoordinates[0]) //start of hiking route
                } else { //end of hiking route
                    drawDirection(routeCoordinates, routeCoordinates[routeCoordinates.length - 1])
                }
            })
        }
    })

    centerArray = routeCoordinates[Math.floor(routeCoordinates.length / 2)]
    activity_center_coordinate['longitude'] = centerArray[0]
    activity_center_coordinate['latitude'] = centerArray[1]
    let entireName = route.rows[0].name
    separator = entireName.indexOf(" ")
    routeNameChinese = entireName.substring(0, separator)
    routeNameEnglish = entireName.substring(separator, entireName.length)
    document.querySelector(".route_name").innerText += `${routeNameChinese}\n${routeNameEnglish}`
    let description = ""
    description += route.rows[0].description + "\n"
    for (var i in route.rows[0].notes) {
        if (i != "短評") {
            description += `\n${i}: `
            description += route.rows[0].notes[i]
        } else {
            description += "\n" + route.rows[0].notes[i] + "\n"
        }
    }
    document.querySelector(".description").innerText = description
    let diff_level = route.rows[0].difficulty
    var buttons = document.querySelectorAll(".button")
    for (button of buttons) {
        button.classList.add(`b${parseInt(diff_level)}`)
    }
    var difficulty = document.querySelector('.difficulty')
    difficulty.classList.add(`b${parseInt(diff_level)}`)
    document.querySelector('.digit').innerHTML = route.rows[0].dimension["Total_distance"]
    document.querySelector('.rewards').innerHTML = `Clover: ${parseInt(diff_level)*5}<br>EXP: ${parseInt(diff_level)*100}`
    for (let i = 0; i < 5; i++) {
        if (diff_level > 1) {
            difficulty.innerHTML += '<i class="fas fa-star star"></i>'
        } else if (diff_level == 1) {
            difficulty.innerHTML += '<i class="fas fa-star-half-alt star"></i>'
        } else {
            difficulty.innerHTML += '<i class="far fa-star star"></i>'
        }
        diff_level -= 2;
    }
    var maxZ = route.rows[0].dimension["Max_elevation"]
    var minZ = route.rows[0].dimension["Min_elevation"]
    var climb = route.rows[0].dimension["Total_climbing"]
    var descent = route.rows[0].dimension["Total_descent"]
    document.querySelector(".elevations").innerHTML = `
    <table style="width:100%">
        <tr>
            <td><i class="fas fa-mountain"></i> : ${maxZ}m</td>
            <td><i class="fas fa-caret-up"></i> : ${climb}m </td>
            <td><i class="fas fa-caret-down"></i> : ${descent}m</td>
        </tr>
    </table>`
    document.querySelector("#button_home").addEventListener('click', function () {
        window.location = `/index/index.html`;
    })
}
loadRoute()


const haveImg = function (propic) {
    if (propic) {
        return `../uploads/${propic}`;
    } else {
        return `../uploads/inv_misc_questionmark.jpg`;
    }
};

async function loadActivities() {
    const res = await fetch(`/activity/${routeID}`);
    const allInfo = await res.json()
    let activities = allInfo[0].rows
    let userActivities = ''
    let subjectRouteActivities = ''
    let nearby = {}
    let nearbyRouteActivities = ''
    const checkTeammate = await fetch('/count_participants')
    const checkInfo = (await checkTeammate.json()).rows
    let participantCount = {}
    for (let activityCount of checkInfo) {
        participantCount[activityCount['activity_id']] = activityCount['count']
    }
    for (let activity of activities) {
        if (activity['status'] === 'pending') {
            let guildID = activity['guild_id']
            if (guildID === 0) {
                const res1 = await fetch(`/check_user_is_participant/${activity['id']}`)
                const participantInfo = await res1.json()
                const isTeammate = (participantInfo['rows'][0])['count'] //return 1 or 0, 1 means user is the teammate of the activity
                let splice = activity['date'].split(" ")
                let date = splice[0]
                let time = splice[1]
                let no_of_teammate = 0
                let no_of_participant = participantCount[`${activity['id']}`]
                if (no_of_participant) {
                    no_of_teammate = parseInt(no_of_participant) + 1 /// 1 is the leader of team
                } else {
                    no_of_teammate = 1
                }

                if (activity['route_id'] == routeID) {
                    let team_buttons_innerHTML = ''
                    let userInfo = await loadUserInfo()
                    let userID = userInfo[0]
                    let own_team = false
                    if (activity['leader_id'] === userID || parseInt(isTeammate) === 1) { //check if user is the leader of the activity
                        own_team = true
                        team_buttons_innerHTML = `
                    <div class="view_activity" activityID="${activity['id']}">My Activity</div>
                `
                    } else if (parseInt(isTeammate) == 1) { console.log("isteammate") }
                    // else if () {}

                    else {
                        const checkRequest = await fetch(`/request_check/${activity['id']}`)
                        const request = await checkRequest.json();
                        if (request.rows.length === 0) {
                            team_buttons_innerHTML = `
                    <div class="team_button join" activityID=${activity['id']}>Request</div>
                    <div class="team_button message">Contact</div>`
                        } else {
                            team_buttons_innerHTML = `
                    <div class="team_button requested" activityID=${activity['id']}>Requested</div>
                    <div class="team_button message">Contact</div>`
                        }
                    }
                    subjectRouteHTML = `
            <div class="team ${own_team ? 'own' : ''}">
                <div class="image_profile" style="background-image: url(${haveImg(activity.profilepic)})"></div>
                <div class="teaminfo_container">
                  <div class="info_text_team">${activity['nickname']}'s Team</div>
                  <div class="info_text_team team_description">${activity['description']}</div>
                </div>
                <div class="timeinfo_container">
                    <div class="info_text_time">${date}</div>
                    <div class="info_text_time">${time}</div>
                </div>
                <div class="no_of_teammate">${no_of_teammate}/${activity['team_size']}</div>
                <div class="team_buttons">
                    ${team_buttons_innerHTML}
                </div>
            </div>`
                    if (own_team) { userActivities += subjectRouteHTML } else { subjectRouteActivities += subjectRouteHTML }
                } else {
                    let routeName = (activity['name'].split(" "))[0]
                    let kmAway = Math.round(allInfo[1][activity['route_id']] * 1) / 1
                    let kmAwayText = kmAway
                    if (kmAway < 0.1) { kmAwayText = 'same path' } else { kmAwayText += 'km away' }
                    nearby[parseInt(kmAway)] = `
            <div class="team nearby">
                <div class="image_profile" style="background-image: url(${haveImg(activity.profilepic)})"></div>
                <div class="teaminfo_container">
                  <div class="info_text_team">${activity['nickname']}'s Team</div>
                  <div class="nearby_route">
                    <div class="nearby_route_name">${routeName}</div>
                    <div class="nearby_route_away">${kmAwayText}</div>
                  </div>
                </div>
                <div class="timeinfo_container">
                    <div class="info_text_time">${date}</div>
                    <div class="info_text_time">${time}</div>
                </div>
                <div class="no_of_teammate">${no_of_teammate}/${activity['team_size']}</div>
                <div class="team_buttons">
                    <div class="view_route" routeID="${activity['route_id']}">View<br>Route</div>
                </div>
            </div>`
                }
            }
        }
    }
    const orderedTeams = Object.keys(nearby).sort().reduce(
        (obj, key) => {
            obj[key] = nearby[key];
            return obj;
        },
        {}
    );
    for (let i in orderedTeams) {
        nearbyRouteActivities += orderedTeams[i]
    }
    document.querySelector(".other_activities").innerHTML = userActivities + subjectRouteActivities + nearbyRouteActivities
    let allViewRoute = document.querySelectorAll(".view_route")
    for (let route of allViewRoute) {
        route.addEventListener('click', function (event) {
            window.location = (`/route/route.html?routeID=${event.currentTarget.getAttribute('routeID')}`)
        })
    }
    let allViewActivity = document.querySelectorAll(".view_activity")
    for (let activity of allViewActivity) {
        activity.addEventListener('click', function (event) {
            let activityID = event.currentTarget.getAttribute('activityID')
            window.location = `/activityinfo/activityinfo.html?activityID=${activityID}`;
        })
    }
    let allRequest = document.querySelectorAll('.join')
    for (let request of allRequest) {
        request.addEventListener('click', async function (event) {
            event.currentTarget.innerText = 'Requested'
            event.currentTarget.classList.add('requested')
            let activityID = event.currentTarget.getAttribute('activityID')
            // const checkRequest = await fetch(`/request_check/${activityID}`)
            // const request = await checkRequest.json();
            const res = await fetch(`/request_team/${activityID}`, {
                method: "POST"
            });
            if (res.status === 200) {
                console.log(await res.json())
            }

            socket.emit("request", 'request sent')
        })
    }
}
loadActivities()

function desExpand() {
    var extend = document.querySelector('#extend')
    if (extend) {
        extend.addEventListener('click', function (event) {
            if (extend.innerHTML === "Extend") {
                document.querySelector(".description").classList.add("expand")
                extend.innerHTML = "Shorten"
            } else {
                document.querySelector(".description").classList.remove("expand")
                extend.innerHTML = "Extend"
            }
        })
    } else {
        console.log('no')
    }
}
let coordinate = []


////////////////mapBox////////////
function loadMapBox(route, route2) {
    mapboxgl.accessToken = 'pk.eyJ1IjoianMxMzk0MzciLCJhIjoiY2ttcnh0NGsyMGNkYTJvb3liYjU1a2V3cyJ9.BXthxGVcKZtAudZFTfovWA';
    var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v11',
        center: route[Math.floor(route.length / 2)],//lng,lat
        zoom: 12
    });
    map.on('load', function () {
        map.addSource('route', {
            'type': 'geojson',
            'data': {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    'type': 'LineString',
                    'coordinates': route
                }
            }
        });
        map.addSource('route2', {
            'type': 'geojson',
            'data': {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    'type': 'LineString',
                    'coordinates': route2
                }
            }
        });
        map.addLayer({
            'id': 'route',
            'type': 'line',
            'source': 'route',
            'layout': {
                'line-join': 'round',
                'line-cap': 'round'
            },
            'paint': {
                'line-color': 'blue',
                'line-width': 5,
                'line-opacity': 0.5
            }
        });
        // Add geolocate control to the map.
        map.addLayer({
            'id': 'route2',
            'type': 'line',
            'source': 'route2',
            'layout': {
                'line-join': 'round',
                'line-cap': 'round'
            },
            'paint': {
                'line-color': 'red',
                'line-width': 5,
                'line-opacity': 0.5
            }
        });
    });


    var geolocate = new mapboxgl.GeolocateControl({
        positionOptions: {
            enableHighAccuracy: true
        },
        trackUserLocation: true
    });
    // Add the control to the map.
    map.addControl(geolocate);
    // map.on('load', function () {
    //     geolocate.trigger();
    // });
}
//////////////mapBox////////

function watchID() {
    const watchId = navigator.geolocation.watchPosition(position => {
        const { latitude, longitude } = position.coords;
        console.log('lat' + latitude + 'lng' + longitude + "!!")
        // Show a map centered at latitude / longitude.
    });
    console.log(position.latitude)
}

// watchID()

async function getLocation() {
    return new Promise((res, rej) => {
        navigator.geolocation.getCurrentPosition(res, rej)
    })
}

async function printLocation() {
    let location = await getLocation()
    console.log(location.coords['latitude'] + "" + location.coords['longitude'] + "location")
}

async function drawDirection(route1, route2_destination) {
    let location = await getLocation()
    currentLocation = [location.coords['longitude'], location.coords['latitude']]
    let directions = await getDirection(currentLocation, route2_destination)//lng,lat
    let currentToDestination = directions['routes'][0]['geometry']['coordinates']
    loadMapBox(route1, currentToDestination)
}

printLocation()

/////////////////gogole map///////////
// function initMap() {
//     const directionsService = new google.maps.DirectionsService();
//     const directionsRenderer = new google.maps.DirectionsRenderer();
//     map = new google.maps.Map(document.getElementById("map"), {
//         center: { lat: 22.408003, lng: 114.125350 },
//         zoom: 13,
//     });
//     // map.setCenter({ lat: 22.302711, lng: 114.177216 })
//     map.setMapTypeId('terrain')
//     directionsRenderer.setMap(map);
//     calculateAndDisplayRoute(directionsService, directionsRenderer);
// }

// function calculateAndDisplayRoute(directionsService, directionsRenderer) {
//     const waypts = [{ location: { lat: 22.408003, lng: 114.125350 }, stopover: true }];
//     directionsService.route(
//         {
//             origin: ({ lat: 22.440662, lng: 114.126737 }),
//             destination: ({ lat: 22.395255, lng: 114.108444 }),
//             waypoints: waypts,
//             //optimizeWaypoints: true,
//             travelMode: google.maps.TravelMode.WALKING,
//         },
//         (response, status) => {
//             if (status === "OK" && response) {
//                 directionsRenderer.setDirections(response);
//             } else {
//                 window.alert("Directions request failed due to " + status);
//             }
//         }
//     );
// }
////////////////google map//////////

function createActivityButtonAction() {
    if (guild === false) {
        document.querySelector(".create_button").addEventListener('click', function () { createForm() })
    } else {
        createForm()
    }
    console.log(form_exist)
    if (form_exist === true) {
        console.log('yes')
    }
}

async function createForm() {
    form_exist = true;
    let userInfo = await loadUserInfo()
    let guildId = userInfo[2]
    console.log(guildId)
    let guildInnerHTML = ''
    if (guildId) {
        guildInnerHTML = `<option value="1">Guild</option>`
    }
    document.querySelector(".large_container").innerHTML = `
        <div class="event_container">
            <form id="activity-form" action="/activity" method="POST" enctype="multipart/form-data">
                <input type="date" id="eventdate" name="eventDate">
                <input type="time" id="eventtime" name="eventTime">
                <textarea id="descriptiontyped" placeholder="Description" name="description"></textarea>
                <select name="teamsize" id="teamsize">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                </select>
                <input type="submit"  id="submit" value="Create">
                <select name="public" id="public">
                    <option value="0">Public</option>
                    ${guildInnerHTML}
                </select>
            </form>
            <div class="cancel"><i class="fas fa-dumpster"></i></div>
        </div>`
    document.querySelector('#activity-form')
        .addEventListener('submit', async function (event) {
            event.preventDefault();
            const form = event.target;
            const formData = new FormData();
            // var dateDigit = form.eventDate.value.split(/\D/)
            // var timeDigit = form.eventTime.value.split(/\D/)
            // var activityDate = new Date(dateDigit[0],dateDigit[1]-1,dateDigit[2])
            // activityDate.setHours(timeDigit[0])
            // activityDate.setMinutes(timeDigit[1])
            formData.routeID = routeID
            formData.date = `${form.eventDate.value} ${form.eventTime.value}`
            formData.description = form.description.value
            formData.teamsize = form.teamsize.value
            formData.public = parseInt(form.public.value)
            formData.guild = parseInt(form.public.value)
            formData.center_coordinate = activity_center_coordinate

            const res = await fetch('/activity', {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(formData)
            });

            if (res.status === 200) {
                console.log(await res.json())
            }
            location.reload()
        })
    document.querySelector(".cancel").addEventListener('click', function () { location.reload() })

}


async function weatherForcast() {
    var res = await fetch('https://data.weather.gov.hk/weatherAPI/opendata/weather.php?dataType=fnd&lang=en')
    var forecast = await res.json();
    // const notification = document.querySelector('#head_info');
    // console.log(JSON.stringify(forcast))
    return forecast
}

async function headerLogWeather() {
    let weatherForecast = await weatherForcast()
    let generalSituation = weatherForecast['generalSituation']
    let futureForecasts = weatherForecast['weatherForecast']
    let today = new Date()
    let todayNums = (today.toLocaleDateString()).split('/')
    let day = (parseInt(todayNums[0][0]) == 0 ? todayNums[0][1] : todayNums[0])
    let month = (parseInt(todayNums[1][0]) == 0 ? todayNums[1][1] : todayNums[1])
    document.querySelector('.today_weather').innerText = `${day + "/" + month}`
    document.querySelector('.today_weather_details').innerText = `${generalSituation}`
    // console.log(JSON.stringify(generalSituation) + "generalSituation")
    let forecastInnerHTML = ''
    for (let forecast of futureForecasts) {
        let forecastDay = forecast['forecastDate'].substring(4, 6)
        let forecastMonth = forecast['forecastDate'].substring(6, 8)
        forecastsDay = ((forecastDay[0] == 0) ? forecastDay[1] : forecastDay)
        forecastMonth = ((forecastMonth[0] == 0) ? forecastMonth[1] : forecastMonth)
        let forecastWeek = forecast['week']
        let iconID = forecast['ForecastIcon']
        // console.log(forecastDay + '/' + forecastMonth)
        forecastInnerHTML += `<div class="day">
                                <div class="weather_day">
                                  <div class="date">${forecastDay}/${forecastMonth}<br>${forecastWeek}</div>
                                  <img src="../weather_icons/pic${iconID}.png" class="weather_image" width="26" height="26">
                                </div>
                              </div>`
        document.querySelector('.weather_forecast').innerHTML = forecastInnerHTML
        // console.log(JSON.stringify(forecast) + "forecast")
    }
}
headerLogWeather()
//desExpand();

function jsonTolnglat(json) {
    let coordinate = []
    var jsonCoords = json
    var lnglat = []
    for (let jsonCoord of jsonCoords) {
        coordinate = []
        coordinate[0] = Number(jsonCoord.longitude)
        coordinate[1] = Number(jsonCoord.latitude)
        lnglat.push(coordinate)
    }
}


async function loadUserInfo() {
    const nameContainer = document.querySelector("#head_info");
    const bottomContainer = document.querySelector(".bottom_button")
    const res = await fetch("/index");
    const result = await res.json();
    if (res.status === 200) {
        data = result;
        // nameContainer.innerHTML = `<h1>${data.id}+${data.nickname}</h1>`;
        bottomContainer.innerHTML = `${data.nickname}`
        return [parseInt(data.id), (data.nickname + ""), parseInt(data.guild_id)]
    } else {
        console.log('no info')
    }
}

loadUserInfo()


async function getDirection(start, end) { //lng,lat
    var res = await fetch(`https://api.mapbox.com/directions/v5/mapbox/walking/${start[0]},${start[1]};${end[0]},${end[1]}?geometries=geojson&access_token=pk.eyJ1IjoianMxMzk0MzciLCJhIjoiY2ttcnh0NGsyMGNkYTJvb3liYjU1a2V3cyJ9.BXthxGVcKZtAudZFTfovWA`)
    var direction = await res.json();
    // const notification = document.querySelector('#head_info');
    // console.log(JSON.stringify(direction) + "direction")
    return direction;
}

async function getThread() {
    console.log(routeID)
    const res = await fetch(`/chatFilteredThread/${routeID}`)
    const threadInfo = (await res.json()).rows
    let subjectRouteInner = ''
    let districtRouteInner = ''
    for (let thread of threadInfo) {
        let title = thread['title']
        let routeName = thread['name']
        let time = thread['created_at'].substring(0, 10)
        let district = thread['district']
        let thread_id = thread['id']
        let route_id = thread['route_id']
        if (route_id = routeID) {
            subjectRouteInner += `<div class="thread" threadID = ${thread_id}>
            <div class="thread_creator">
              <div>${routeName}</div>
              <div>${time}</div>
            </div>
            <div class="thread_topic">${title}</div>
          </div>`
        } else {
            districtRouteInner += `<div class="thread" threadID = ${thread_id}>
            <div class="thread_creator">
              <div>${routeName}</div>
              <div${time}</div>
            </div>
            <div class="thread_topic">${title}</div>
          </div>`
        }
    }
    document.querySelector('.related_thread').innerHTML = subjectRouteInner + districtRouteInner
    let threads = document.querySelectorAll('.thread')
    for (let thread of threads) {
        thread.addEventListener('click', function (event) {
            let threadID = event.currentTarget.getAttribute('threadID')
            window.location = `/mainforum/forumthread.html?id=${threadID}`
        })
    }
    // console.log(JSON.stringify(threadInfo)+"info")

    // document.querySelector('.')

}

getThread()


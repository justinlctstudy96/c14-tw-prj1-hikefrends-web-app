document.querySelector(".reg").addEventListener("click", () => {
  window.location = "/register/register.html";
});

document
  .querySelector("#login-form")
  .addEventListener("submit", async (event) => {
    event.preventDefault();

    const form = event.target;
    const loginInfo = {};
    loginInfo.email = form.email.value;
    loginInfo.password = form.password.value;

    const res = await fetch("/login", {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify(loginInfo),
    });

    if (res.status === 200) {
      const result = await res.json();
      console.log(result);
      window.location = "/index/index.html";
    } else {
      document.querySelector(".formHeading").innerHTML = ` <h1>山友</h1>`;
      const h3 = document.createElement("h3");
      h3.classList.add("wrongNotice");
      h3.innerHTML = `Wrong username / password`;

      document.querySelector(".formHeading").appendChild(h3);
      console.log("Wrong username / password");
    }

    form.reset();
  });

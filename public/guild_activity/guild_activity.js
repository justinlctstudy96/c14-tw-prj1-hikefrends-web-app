// var guildID = (document.location.href).split('?')[1].split('=')[1]

loadGuildInfo()
loadGuildTeams()

document.querySelector("#button_home").addEventListener('click', function () {
    window.location = `/index/index.html`;
})

const haveImg = function (propic) {
    if (propic) {
        return `../uploads/${propic}`;
    } else {
        return '../uploads/inv_misc_questionmark.jpg';
    }
};

async function loadGuildInfo() {
    const res = await fetch(`/guild_info`);
    const result = await res.json();
    let guild_name = result['guild_info']['name']
    document.querySelector('#head_info').innerHTML += guild_name
    document.querySelector(".back").addEventListener('click', async function () {
        const res = await fetch("/index");
        if (res.status == 200) {
            window.history.back();
        }
    })
}

async function loadGuildTeams() {
    let userInfo = await loadUserInfo()
    let guildID = userInfo[2]
    const formData = new FormData();
    formData.filter = "Guild"
    // console.log(JSON.stringify(formData))
    const res = await fetch('/allactivities', {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(formData)
    });
    const allInfo = await res.json()
    console.log(allInfo)


    const checkTeammate = await fetch('/count_participants')
    const checkInfo = (await checkTeammate.json()).rows
    let participantCount = {}
    for (let activityCount of checkInfo) {
        participantCount[activityCount['activity_id']] = activityCount['count']
    }


    let activities = allInfo.rows
    let activitiesSortedByDistrict = {}
    for (let activity of activities) {
        if (activity['status'] === 'pending') {
            console.log(activity['guild_id'])
            console.log(guildID)
            if (activity['guild_id'] === guildID) {
                let splice = activity['date'].split(" ")
                let date = splice[0]
                let time = splice[1]
                const res1 = await fetch(`/check_user_is_participant/${activity['activity_id']}`)
                const participantInfo = await res1.json()
                const isTeammate = (participantInfo['rows'][0])['count'] //return 1 or 0, 1 means user is the teammate of the activity
                let no_of_teammate = 0
                let no_of_participant = participantCount[`${activity['activity_id']}`]
                if (no_of_participant) {
                    no_of_teammate = parseInt(no_of_participant) + 1 /// 1 is the leader of team
                } else {
                    no_of_teammate = 1
                }

                // for (let index in activity['participant_id']) {
                //     no_of_teammate++;
                // }
                // if (no_of_teammate === activity['team_size']) {
                //     break;
                // }
                let team_buttons_innerHTML = ''
                let userInfo = await loadUserInfo()
                let userID = userInfo[0]
                let own_team = false
                let routeName = (activity['name'].split(" "))[0]
                if (activity['leader_id'] === userID || parseInt(isTeammate) === 1) {
                    own_team = true
                    team_buttons_innerHTML = `
                        <div class="view_activity" activityID="${activity['activity_id']}">Guild Activity</div>`
                } else {
                    // team_buttons_innerHTML = `
                    //     <div class="view_route" routeID="${activity['route_id']}">View<br>Route</div>`
                    const checkRequest = await fetch(`/request_check/${activity['activity_id']}`)
                    const request = await checkRequest.json();
                    if (request.rows.length === 0) {
                        team_buttons_innerHTML = `
                    <div class="team_button join" activityID=${activity['activity_id']}>Request</div>
                    <div class="team_button message">Contact</div>`
                    } else {
                        team_buttons_innerHTML = `
                    <div class="team_button requested" activityID=${activity['activity_id']}>Requested</div>
                    <div class="team_button message">Contact</div>`
                    }

                }
                activityHTML = `
                    <div class="team ${own_team ? 'own' : `d${parseInt(activity['difficulty'])}`}">
                        <div class="image_profile" style="background-image: url(${haveImg(activity.profilepic)}"></div>
                        <div class="teaminfo_container">
                          <div class="info_text_team">${activity['nickname']}'s Team</div>
                          <div class="nearby_route">
                            <div class="nearby_route_name">${routeName}</div>
                          </div>
                        </div>
                        <div class="timeinfo_container">
                            <div class="info_text_time">${date}</div>
                            <div class="info_text_time">${time}</div>
                        </div>
                        <div class="no_of_teammate">
                            ${(no_of_teammate === activity['team_size']) ? 'Full' : (no_of_teammate + '/' + activity['team_size'])}
                        </div>
                        <div class="team_buttons">
                            ${team_buttons_innerHTML}
                        </div>
                    </div>`
                if (activitiesSortedByDistrict[activity['district']]) {
                    activitiesSortedByDistrict[activity['district']].push(activityHTML)
                } else {
                    activitiesSortedByDistrict[activity['district']] = [activityHTML]
                }
            }
        }
    }
    let inner = ""
    for (let district in activitiesSortedByDistrict) {
        inner += `<div class="difficulty easy">${district}</div>`
        for (let activity of activitiesSortedByDistrict[district]) {
            inner += activity
        }
    }
    document.querySelector("#page_content_container").innerHTML = `<div class="activities_container">${inner}</div>`


    let allViewRoute = document.querySelectorAll(".view_route")
    for (let route of allViewRoute) {
        route.addEventListener('click', function (event) {
            window.location = (`/route/route.html?routeID=${event.currentTarget.getAttribute('routeID')}`)
        })
    }
    let allViewActivity = document.querySelectorAll(".view_activity")
    for (let activity of allViewActivity) {
        activity.addEventListener('click', function (event) {
            let activityID = event.currentTarget.getAttribute('activityID')
            window.location = `/activityinfo/activityinfo.html?activityID=${activityID}`;
        })
    }
    let allRequest = document.querySelectorAll('.join')
    for (let request of allRequest) {
        request.addEventListener('click', async function (event) {
            event.currentTarget.innerText = 'Requested'
            event.currentTarget.classList.add('requested')
            let activityID = event.currentTarget.getAttribute('activityID')
            // const checkRequest = await fetch(`/request_check/${activityID}`)
            // const request = await checkRequest.json();
            const res = await fetch(`/request_team/${activityID}`, {
                method: "POST"
            });
            if (res.status === 200) {
                console.log(await res.json())
            }

            socket.emit("request", 'request sent')
        })
    }
}

async function loadUserInfo() {
    const nameContainer = document.querySelector("#head_info");
    const bottomContainer = document.querySelector(".bottom_button")
    const res = await fetch("/index");
    const result = await res.json();
    if (res.status === 200) {
        data = result;
        // nameContainer.innerHTML = `<h1>${data.id}+${data.nickname}</h1>`;
        bottomContainer.innerHTML = `${data.nickname}`
        console.log([parseInt(data.id), (data.nickname + ""), parseInt(data.guild_id)])
        return [parseInt(data.id), (data.nickname + ""), parseInt(data.guild_id)]
    } else {
        console.log('no info')
    }
}

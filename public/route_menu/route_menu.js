


var filter = "Difficulty"

checkingHikingActivity()



async function loadRoutes(filter) {
    const res = await fetch('/loadroutes');
    const routes = await res.json()
    console.log(routes.rows[2])
    switch (filter) {
        case "Difficulty":
            document.querySelector("#page_content_container").innerHTML = `
                <div id="easy" class="difficulty_container">
                    <div class="difficulty easy"><i class="far fa-star star"></i></div>
                    <div class="icons-container">
                        <div id="row0" class="icons-row"></div>
                        <div id="row1" class="icons-row"></div>
                    </div>
                    <img src="./images/easy.jpeg" class="img_fluid">
                </div>
                <div id="medium" class="difficulty_container">
                    <div class="difficulty medium" style="z-index: 10;"><i class="fas fa-star star"></i><i
                        class="fas fa-star star"></i></div>
                    <div class="icons-container">
                        <div id="row0" class="icons-row"></div>
                        <div id="row1" class="icons-row"></div>
                    </div>
                    <img src="./images/medium.jpeg" class="img_fluid">
                </div>
                <div id="hard" class="difficulty_container">
                    <div class="difficulty hard"><i class="fas fa-star star"></i><i class="fas fa-star star"></i><i
                        class="fas fa-star star"></i><i class="fas fa-star-half-alt star"></i></div>
                    <div class="icons-container">
                        <div id="row0" class="icons-row"></div>
                        <div id="row1" class="icons-row"></div>
                    </div>
                    <img src="./images/hard.jpeg" class="img_fluid" style>
                </div>`
            var div = $(".icons-container")
            var scrollto = div.offset().left + (div.width() / 1.48);
            div.animate({ scrollLeft: scrollto })
            let easyContainer = document.querySelector('#easy')
            let mediumContainer = document.querySelector('#medium')
            let hardContainer = document.querySelector('#hard')
            count = [0, 0, 0]
            for (route of routes.rows) {
                if (route["difficulty"] <= 4) {
                    easyContainer.querySelector(`#row${count[0] % 2}`).innerHTML +=
                        `<div routeID=${route['id']} class="route-icon b${parseInt(route["difficulty"])}" style="background-image: url('../route_pic/${route['name']}.jpg')">
                            <div class="route-name">${route['name'].substring(0, route['name'].indexOf(" "))}</div>
                        </div>`
                    count[0]++;
                } else if (route["difficulty"] <= 7) {
                    mediumContainer.querySelector(`#row${count[1] % 2}`).innerHTML +=
                        `<div routeID=${route['id']} class="route-icon b${parseInt(route["difficulty"])}" style="background-image: url('../route_pic/${route['name']}.jpg')">
                            <div class="route-name">${route['name'].substring(0, route['name'].indexOf(" "))}</div>
                        </div>`
                    count[1]++;
                } else {
                    hardContainer.querySelector(`#row${count[2] % 2}`).innerHTML +=
                        `<div routeID=${route['id']} class="route-icon b${parseInt(route["difficulty"])}" style="background-image: url('../route_pic/${route['name']}.jpg')">
                            <div class="route-name">${route['name'].substring(0, route['name'].indexOf(" "))}</div>
                        </div>`
                    count[2]++;
                }
            }
            break;
        case "District":
            let routesSortedByDistrict = {}
            for (route of routes.rows) {
                let routeHTML = `<div routeID=${route['id']} class="route-icon district-icon b${parseInt(route["difficulty"])}" style="background-image: url('../route_pic/${route['name']}.jpg')">
                                    <div class="route-name">${route['name'].substring(0, route['name'].indexOf(" "))}</div>
                                </div>`
                if (routesSortedByDistrict[route['district']]) {
                    routesSortedByDistrict[route['district']].push(routeHTML)
                } else {
                    routesSortedByDistrict[route['district']] = [routeHTML]
                }
            }
            let innerDistrict = ""
            for (let district in routesSortedByDistrict) {
                innerDistrict += `<div class="district_container">`
                innerDistrict += `<div class="difficulty easy">${district}</div>`
                for (let route of routesSortedByDistrict[district]) {
                    innerDistrict += route
                }
                innerDistrict += `</div>`
            }
            document.querySelector("#page_content_container").innerHTML = `<div class="activities_container">${innerDistrict}</div>`
            break;
        case "Distance":
            let routesSortedByDistance = { "1": [], "2": [], "4": [], "6": [], "8": [], "10": [], "12": [], "14": [], "16": [], "18": [], "20": [] }
            for (route of routes.rows) {
                let routeHTML = `<div routeID=${route['id']} class="route-icon district-icon b${parseInt(route["difficulty"])}" style="background-image: url('../route_pic/${route['name']}.jpg')">
                                    <div class="route-name">${route['name'].substring(0, route['name'].indexOf(" "))}</div>
                                </div>`
                let distance = route['dimension']['Total_distance']
                if (distance <= 2) {
                    routesSortedByDistance['1'].push(routeHTML)
                } else if (distance <= 4) {
                    routesSortedByDistance['2'].push(routeHTML)
                } else if (distance <= 6) {
                    routesSortedByDistance['4'].push(routeHTML)
                } else if (distance <= 8) {
                    routesSortedByDistance['6'].push(routeHTML)
                } else if (distance <= 10) {
                    routesSortedByDistance['8'].push(routeHTML)
                } else if (distance <= 12) {
                    routesSortedByDistance['10'].push(routeHTML)
                } else if (distance <= 14) {
                    routesSortedByDistance['12'].push(routeHTML)
                } else if (distance <= 16) {
                    routesSortedByDistance['14'].push(routeHTML)
                } else if (distance <= 18) {
                    routesSortedByDistance['16'].push(routeHTML)
                } else if (distance <= 20) {
                    routesSortedByDistance['18'].push(routeHTML)
                } else {
                    routesSortedByDistance['20'].push(routeHTML)
                }
            }
            let innerDistance = ""
            for (let distance in routesSortedByDistance) {
                innerDistance += `<div class="district_container">`
                innerDistance += `<div class="difficulty easy"><i class="fas fa-angle-right"></i>${distance}km</div>`
                for (let route of routesSortedByDistance[distance]) {
                    innerDistance += route
                }
                innerDistance += `</div>`
            }
            document.querySelector("#page_content_container").innerHTML = `<div class="activities_container">${innerDistance}</div>`
            break;
        case "Elevation":
            let routesSortedByElevation = { "300": [], "400": [], "500": [], "600": [], "700": [], "800": [], "900": [] }
            for (route of routes.rows) {
                let routeHTML = `<div routeID=${route['id']} class="route-icon district-icon b${parseInt(route["difficulty"])}" style="background-image: url('../route_pic/${route['name']}.jpg')">
                                    <div class="route-name">${route['name'].substring(0, route['name'].indexOf(" "))}</div>
                                </div>`
                let max_elevation = route['dimension']['Max_elevation']
                if (max_elevation <= 400) {
                    routesSortedByElevation['300'].push(routeHTML)
                } else if (max_elevation <= 500) {
                    routesSortedByElevation['400'].push(routeHTML)
                } else if (max_elevation <= 600) {
                    routesSortedByElevation['500'].push(routeHTML)
                } else if (max_elevation <= 700) {
                    routesSortedByElevation['600'].push(routeHTML)
                } else if (max_elevation <= 800) {
                    routesSortedByElevation['700'].push(routeHTML)
                } else if (max_elevation <= 900) {
                    routesSortedByElevation['800'].push(routeHTML)
                } else {
                    routesSortedByElevation['900'].push(routeHTML)
                }
            }
            let innerElevation = ""
            for (let elevation in routesSortedByElevation) {
                innerElevation += `<div class="district_container">`
                innerElevation += `<div class="difficulty easy"><i class="fas fa-angle-right"></i>${elevation}m</div>`
                for (let route of routesSortedByElevation[elevation]) {
                    innerElevation += route
                }
                innerElevation += `</div>`
            }
            document.querySelector("#page_content_container").innerHTML = `<div class="activities_container">${innerElevation}</div>`
            break;

    }
    addEventListener()
}


async function loadTeams(filter, center = 0) {
    const formData = new FormData();
    formData.filter = filter
    formData.center = center
    console.log(JSON.stringify(formData))
    const res = await fetch('/allactivities', {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(formData)
    });
    const allInfo = await res.json()


    const checkTeammate = await fetch('/count_participants')
    const checkInfo = (await checkTeammate.json()).rows
    let participantCount = {}
    for (let activityCount of checkInfo) {
        participantCount[activityCount['activity_id']] = activityCount['count']
    }

    if (filter === "District") {
        let activities = allInfo.rows
        let activitiesSortedByDistrict = {}
        for (let activity of activities) {
            if (activity['status'] === 'pending') {
                if (activity['guild_id'] === 0) {
                    let splice = activity['date'].split(" ")
                    let date = splice[0]
                    let time = splice[1]
                    const res1 = await fetch(`/check_user_is_participant/${activity['id']}`)
                    const participantInfo = await res1.json()
                    const isTeammate = (participantInfo['rows'][0])['count'] //return 1 or 0, 1 means user is the teammate of the activity
                    let no_of_teammate = 0
                    let no_of_participant = participantCount[`${activity['activity_id']}`]
                    if (no_of_participant) {
                        no_of_teammate = parseInt(no_of_participant) + 1 /// 1 is the leader of team
                    } else {
                        no_of_teammate = 1
                    }

                    // for (let index in activity['participant_id']) {
                    //     no_of_teammate++;
                    // }
                    // if (no_of_teammate === activity['team_size']) {
                    //     break;
                    // }
                    let team_buttons_innerHTML = ''
                    let userInfo = await loadUserInfo()
                    let userID = userInfo[0]
                    let own_team = false
                    let routeName = (activity['name'].split(" "))[0]
                    if (activity['leader_id'] === userID || parseInt(isTeammate) === 1) {
                        own_team = true
                        team_buttons_innerHTML = `
                        <div class="view_activity" activityID="${activity['activity_id']}">My Activity</div>`
                    } else {
                        team_buttons_innerHTML = `
                        <div class="view_route" routeID="${activity['route_id']}">View<br>Route</div>`
                    }

                    const haveImg = function () {
                        if (activity.profilepic) {
                          return `<div class="image_profile" style="background-image: url('../uploads/${activity.profilepic}')"></div>`;
                        } else {
                          return `<div class="image_profile" style="background-image: url('../uploads/inv_misc_questionmark.jpg')"></div>`;
                        }
                      };


                    activityHTML = `
                    <div class="team ${own_team ? 'own' : `d${parseInt(activity['difficulty'])}`}">
                        ${haveImg()}
                        <div class="teaminfo_container">
                          <div class="info_text_team">${activity['nickname']}'s Team</div>
                          <div class="nearby_route">
                            <div class="nearby_route_name">${routeName}</div>
                          </div>
                        </div>
                        <div class="timeinfo_container">
                            <div class="info_text_time">${date}</div>
                            <div class="info_text_time">${time}</div>
                        </div>
                        <div class="no_of_teammate">
                            ${(no_of_teammate === activity['team_size']) ? 'Full' : (no_of_teammate + '/' + activity['team_size'])}
                        </div>
                        <div class="team_buttons">
                            ${team_buttons_innerHTML}
                        </div>
                    </div>`
                    if (activitiesSortedByDistrict[activity['district']]) {
                        activitiesSortedByDistrict[activity['district']].push(activityHTML)
                    } else {
                        activitiesSortedByDistrict[activity['district']] = [activityHTML]
                    }
                }
            }
        }
        let inner = ""
        for (let district in activitiesSortedByDistrict) {
            inner += `<div class="difficulty easy">${district}</div>`
            for (let activity of activitiesSortedByDistrict[district]) {
                inner += activity
            }
        }
        document.querySelector("#page_content_container").innerHTML = `<div class="activities_container">${inner}</div>`
    } else {
        let activities = allInfo[0].rows
        // console.log(JSON.stringify(allInfo)+"activity")
        let userActivities = ''
        let otherActivities = ''
        let nearby = {}
        for (let activity of activities) {
            if (activity['guild_id'] === 0) {
                let splice = activity['date'].split(" ")
                let date = splice[0]
                let time = splice[1]

                const res1 = await fetch(`/check_user_is_participant/${activity['id']}`)
                const participantInfo = await res1.json()
                const isTeammate = (participantInfo['rows'][0])['count']
                let no_of_teammate = 0
                let no_of_participant = participantCount[`${activity['id']}`]
                if (no_of_participant) {
                    no_of_teammate = parseInt(no_of_participant) + 1 /// 1 is the leader of team
                } else {
                    no_of_teammate = 1
                }
                let team_buttons_innerHTML = ''
                let userInfo = await loadUserInfo()
                let userID = userInfo[0]
                let own_team = false
                let routeName = (activity['name'].split(" "))[0]
                let kmAway = Math.round(allInfo[1][activity['route_id']] * 1) / 1
                let kmAwayText = kmAway
                if (kmAway < 0.1) { kmAwayText = 'same path' } else { kmAwayText += 'km away' }
                if (activity['leader_id'] === userID || parseInt(isTeammate) === 1) {
                    own_team = true
                    team_buttons_innerHTML = `
                        <div class="view_activity" activityID="${activity['id']}">My Activity</div>`
                } else {
                    team_buttons_innerHTML = `
                        <div class="view_route" routeID="${activity['route_id']}">View<br>Route</div>`
                }

                const haveImg = function () {
                    if (activity.profilepic) {
                      return `<div class="image_profile" style="background-image: url('../uploads/${activity.profilepic}')"></div>`;
                    } else {
                      return `<div class="image_profile" style="background-image: url('../uploads/inv_misc_questionmark.jpg')"></div>`;
                    }
                  };

                activityHTML = `
                    <div class="team ${own_team ? 'own' : `d${parseInt(activity['difficulty'])}`}">
                        ${haveImg()}
                        <div class="teaminfo_container">
                          <div class="info_text_team">${activity['nickname']}'s Team</div>
                          <div class="nearby_route">
                            <div class="nearby_route_name">${routeName}</div>
                            <div class="nearby_route_away">${kmAwayText}</div>
                          </div>
                        </div>
                        <div class="timeinfo_container">
                            <div class="info_text_time">${date}</div>
                            <div class="info_text_time">${time}</div>
                        </div>
                        <div class="no_of_teammate">
                            ${(no_of_teammate === activity['team_size']) ? 'Full' : (no_of_teammate + '/' + activity['team_size'])}
                        </div>
                        <div class="team_buttons">
                            ${team_buttons_innerHTML}
                        </div>
                    </div>`
                if (own_team) { userActivities += activityHTML } else { nearby[parseInt(kmAway)] = activityHTML }
            }
        }
        const orderedTeams = Object.keys(nearby).sort().reduce(
            (obj, key) => {
                obj[key] = nearby[key];
                return obj;
            },
            {}
        );
        for (let i in orderedTeams) {
            otherActivities += orderedTeams[i]
        }
        document.querySelector("#page_content_container").innerHTML = `<div class="activities_container">${userActivities + otherActivities}</div>`
    }
    let allViewRoute = document.querySelectorAll(".view_route")
    for (let route of allViewRoute) {
        route.addEventListener('click', function (event) {
            window.location = (`/route/route.html?routeID=${event.currentTarget.getAttribute('routeID')}`)
        })
    }
    let allViewActivity = document.querySelectorAll(".view_activity")
    for (let activity of allViewActivity) {
        activity.addEventListener('click', function (event) {
            let activityID = event.currentTarget.getAttribute('activityID')
            window.location = `/activityinfo/activityinfo.html?activityID=${activityID}`;
        })
    }
}

// if (res.status === 200) {
//     console.log(await res.json())
// }

loadRoutes("Difficulty")

function addEventListener() {

    const route_icons = document.querySelectorAll('.route-icon')
    for (let route_icon of route_icons) {
        route_icon.addEventListener('click', function (event) {
            const index = event.currentTarget.getAttribute('routeID');
            window.location = `../route/route.html?routeID=${index}`;
        })
    }
}
/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
function myFunction1() {
    document.getElementById("myDropdownl").classList.toggle("show");
    let thisBtn = document.querySelector(".dropbtnl")
    thisBtn.innerText = 'Sort by'


    // document.querySelector(".l1").addEventListener('click', function () { console.log("yes") })
}
function myFunction2() {
    document.getElementById("myDropdownr").classList.toggle("show");
    let thisBtn = document.querySelector(".dropbtnr")
    thisBtn.innerText = 'Sort by'


}
const userLocation = navigator.geolocation.getCurrentPosition(position => {
    const { latitude, longitude } = position.coords;
    console.log(latitude)
});
console.log(JSON.stringify(userLocation) + "!!")


let filterList = document.querySelectorAll("a")
let routeClass = document.querySelector(".dropbtnl")
let teamClass = document.querySelector(".dropbtnr")
for (let filterClicked of filterList) {
    filterClicked.addEventListener('click', function (event) {
        let filter = event.currentTarget.innerText
        if (event.currentTarget.classList.contains("routes")) {
            loadRoutes(filter)
            if (!teamClass.classList.contains("unclick")) {
                teamClass.classList.add("unclick")
            }
            if (routeClass.classList.contains("unclick")) {
                routeClass.classList.remove("unclick")
            }
        } else if (event.currentTarget.classList.contains("teams")) {
            if (filter === "Nearby") {
                const userLocation = navigator.geolocation.getCurrentPosition(position => {
                    const { latitude, longitude } = position.coords;
                    let userLocation = [longitude, latitude]
                    loadTeams(filter, userLocation)
                });
            } else {
                loadTeams(filter)
            }
            if (!routeClass.classList.contains("unclick")) {
                routeClass.classList.add("unclick")
            }
            if (teamClass.classList.contains("unclick")) {
                teamClass.classList.remove("unclick")
            }
        }
    })
}

document.querySelector("#button_home").addEventListener('click', function () {
    window.location = `/index/index.html`;
})


// Close the dropdown menu if the user clicks outside of it
window.onclick = function (event) {
    if (!event.target.matches('.dropbtnl') && !event.target.matches('.dropbtnr')) {
        var dropdowns = document.getElementsByClassName("dropdown-contentl");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
        var dropdowns = document.getElementsByClassName("dropdown-contentr");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
        document.querySelector(".dropbtnl").innerText = 'Routes'
        document.querySelector(".dropbtnr").innerText = 'Teams'
    }
}

loadUserInfo()
async function loadUserInfo() {
    const nameContainer = document.querySelector("#head_info");
    const bottomContainer = document.querySelector(".bottom_button")
    const res = await fetch("/index");
    const result = await res.json();
    if (res.status === 200) {
        data = result;
        nameContainer.innerHTML = `<h1>${data.nickname}</h1>`;
        bottomContainer.innerHTML = `${data.nickname}`
        console.log(data.nickname)
        return [parseInt(data.id), (data.nickname + ""), data.profilepic]
    } else {
        console.log('fk')
    }
}

async function checkingHikingActivity() {
    const res = await fetch("/check_isHiking")
    const result = await res.json()
    let leaderActivity = result[0]
    let participantActivity = result[1]
    let hikingActivityID = 0

    // try {
    try {
        if (leaderActivity[0]['id']) {
            hikingActivityID = leaderActivity[0]['id']
        }
    } catch(e){}
    try {
        if (participantActivity[0]['activity_id']) {
            hikingActivityID = participantActivity[0]['activity_id']
        }
    } catch (re){}
    let linkHikingActivityButton = document.querySelector('.hiking')
    if (hikingActivityID != 0) {
        linkHikingActivityButton.setAttribute('activityID', `${hikingActivityID}`)
        // linkHikingActivityButton.classList.add('isHikingBg')
        linkHikingActivityButton.innerHTML = '<i class="fas fa-hiking"></i>'
        setInterval(function () {
            linkHikingActivityButton.classList.toggle('isHiking')
        }, 500)
        linkHikingActivityButton.addEventListener('click', function () {
            window.location = `/activityinfo/activityinfo.html?activityID=${hikingActivityID}`;
        })
    } else {
        linkHikingActivityButton.innerHTML = '<i class="fas fa-bed"></i>'
    }
    // } catch (err) {
    // document.querySelector('.hiking').innerHTML = '<i class="fas fa-bed"></i>'
    // }
}


// $(window).click(function (event) {
//     if (!event.target.matches('.dropbtnl') || !event.target.matches('.dropbtnr')) {
//         document.querySelector(".dropbtnl").innerText = 'Routes'
//         document.querySelector(".dropbtnr").innerText = 'Teams'
//         var dropdowns = document.getElementsByClassName("dropdown-contentl");
//         var i;
//         for (i = 0; i < dropdowns.length; i++) {
//             var openDropdown = dropdowns[i];
//             if (openDropdown.classList.contains('show')) {
//                 openDropdown.classList.remove('show');
//             }
//         }
//         var dropdowns = document.getElementsByClassName("dropdown-contentr");
//         var i;
//         for (i = 0; i < dropdowns.length; i++) {
//             var openDropdown = dropdowns[i];
//             if (openDropdown.classList.contains('show')) {
//                 openDropdown.classList.remove('show');
//             }
//         }


//     }

// setInterval(function(){
//     document.querySelector('.r2').trigger('click')
// },1000)
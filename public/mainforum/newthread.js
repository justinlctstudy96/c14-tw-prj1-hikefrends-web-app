document.querySelector(".return").addEventListener("click", () => {
  // window.location = `/mainforum/forumcat.html`;
  window.history.back();
});

async function selectDistrict() {
  const select = document.querySelector(".selectDistrict");
  const res = await fetch(`/allCat`);
  const result = await res.json();

  if (res.status === 200) {
    data = result;
    for (let i = 0; i < data.rows.length; i++) {
      select.innerHTML += `<option class="selectOption" value="${data.rows[i].district}">${data.rows[i].district}</option>`;
    }
  }
}
selectDistrict();

document
  .querySelector(".selectDistrict")
  .addEventListener("change", async () => {
    const res = await fetch(`/getDistrictRoute/${event.target.value}`);
    const result = await res.json();
    const selectRoute = document.querySelector(".selectRoute");
    selectRoute.innerHTML = "";
    if (res.status === 200) {
      data = result;
      for (let i = 0; i < data.rows.length; i++) {
        selectRoute.innerHTML += `<option class="selectOption" value="${data.rows[i].id}">${data.rows[i].name}</option>`;
      }
    }
  });

document
  .querySelector("#textarea-form")
  .addEventListener("submit", async (event) => {
    event.preventDefault();

    const form = event.target;
    const formData = new FormData();

    formData.append("content", form.content.value);
    formData.append("title", form.title.value);
    formData.append("routeName", form.routeName.value);
    if (form.image.files[0]) {
      formData.append("image", form.image.files[0]);
    }

    const res = await fetch(`/newThread`, {
      method: "POST",
      body: formData,
    });

    const result = await res.json();
    const id = result.rows[0].id;
    console.log(id);

    form.reset();
    window.location = `/mainforum/forumthread.html?id=${id}`;
  });

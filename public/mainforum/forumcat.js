document.querySelector(".return").addEventListener("click", () => {
  window.location = "/index/index.html";
});

document.querySelector(".fa-plus").addEventListener("click", () => {
  window.location = "/mainforum/newthread.html";
});

//由thread table 到拎返 thread既 title upvote created_at
async function loadAllThread() {
  const res = await fetch("/allThread");
  const result = await res.json();
  const main = document.querySelector("main");

  if (res.status === 200) {
    data = result;
    //loop all the entity and remeber class id collapse id is the route id

    for (let i = 0; i < data.rows.length; i++) {
      main.innerHTML += `<div class="card card-body" id="${data.rows[i].id}">
     <div class="topic">
     <div class="title">${data.rows[i].title}</div>
       <div class="time-span">${
         data.rows[i].max_updated_at || data.rows[i].created_at
       }</div>
     </div>

     <div class="left-control">
     <div class="route-name">${data.rows[i].name}</div>
     <div class="like">
        <i class="fas fa-thumbs-up"></i><span>${data.rows[i].upvote}</span>
     </div>
     </div>
   </div>
 </div>`;
    }
    let allCards = document.querySelectorAll(".card");
    for (let card of allCards) {
      card.addEventListener("click", function (event) {
        console.log(`${event.currentTarget.getAttribute("id")}`);
        window.location = `/mainforum/forumthread.html?id=${event.currentTarget.getAttribute(
          "id"
        )}`;
      });
    }
    listening();
  }
}

async function toggle() {
  const cat = document.querySelector(".cat");
  if (cat.style.display === "none") {
    cat.style.display = "flex";
  } else {
    cat.style.display = "none";
  }
}

async function backToAll() {
  const main = document.querySelector("main");
  main.innerHTML = `<div class="control">
      <h2>所有帖</h2>
    </div>
    <div class="cat" style="display: none;"></div>`;
  loadCat();
  loadAllThread();
}

async function loadCat() {
  const res = await fetch("/allCat");
  const result = await res.json();
  const main = document.querySelector("main");
  const cat = document.querySelector(".cat");
  if (res.status === 200) {
    data = result;
    for (let i = 0; i < data.rows.length; i++) {
      cat.innerHTML += `<div class="btn btn-light cate" id="${data.rows[i].district}">${data.rows[i].district}</div>`;
    }
    listening();
  }
}
loadCat();
function listening() {
  let allCats = document.querySelectorAll(".cate");
  for (let cat of allCats) {
    cat.addEventListener("click", async function (event) {
      const id = event.currentTarget.getAttribute("id");
      console.log(id);
      window.location = `/mainforum/forumcat.html?id=${event.currentTarget.getAttribute(
        "id"
      )}`;
    });
  }
}

async function accordingToId() {
  const id = decodeURI(document.location.href.split("?")[1].split("=")[1]);
  const res = await fetch(`/filtered/${id}`);
  console.log(id);
  const result = await res.json();

  const main = document.querySelector("main");
  if (res.status === 200) {
    data = result;
    main.innerHTML = `<div class="control">
      <h2>${id}</h2>
    </div>
    <div class="cat" style="display: none;"></div>`;
    loadCat();
    for (let i = 0; i < data.rows.length; i++) {
      main.innerHTML += `<div class="card card-body" id="${data.rows[i].id}">
     <div class="topic">
     <div class="title">${data.rows[i].title}</div>
       <div class="time-span">${
         data.rows[i].max_updated_at || data.rows[i].created_at
       }</div>
     </div>

     <div class="left-control">
     <div class="route-name">${data.rows[i].name}</div>
     <div class="like">
        <i class="fas fa-thumbs-up"></i><span>${data.rows[i].upvote}</span>
     </div>
     </div>
   </div>
 </div>`;
    }
    let allCards = document.querySelectorAll(".card");
    for (let card of allCards) {
      card.addEventListener("click", function (event) {
        console.log(`${event.currentTarget.getAttribute("id")}`);
        window.location = `/mainforum/forumthread.html?id=${event.currentTarget.getAttribute(
          "id"
        )}`;
      });
    }
    listening();
  }
}
loadAllThread();
setTimeout(() => {
  accordingToId();
}, 100);

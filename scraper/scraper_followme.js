// Import the playwright and other required modules.
const playwright = require('playwright');
const fs = require('fs');
const fetch = require('node-fetch')

//import module functions
const region = require('./scraper_getRegionLinks');
const links = require('./scraper_getPageLinks');
const data = require('./scraper_getLinkData');
const GPX = require('./scraper_getGPXData');
const pic = require('./scraper_getPic');
const download = require('./scraper_downloadFile')


//Extract list of links regions
async function list_generate(path) {
    const region_links = await region.get_regions(path);
    for (key in region_links) {
        await links.get_links(region_links[key], key);
    }

};

//Read all page links extracted
const lists = JSON.parse(fs.readFileSync('./data/links.json'))

async function scrap_all_route() {
    let i = 0;
    for (district in lists) {
        for (link of lists[district]) {
            try {
                i++;
                const route_data = await data.getData(link, district);
                await fs.writeFile(`./data/route_info/route_download_log.json`, `${i}: Route data downloaded: ${route_data['name']}\n`, { flag: 'a' }, (err) => {
                    if (err) throw err;
                    console.log(`${i}: Route data downloaded: ${route_data['name']}`)
                });

            } catch (error) {
                console.log(error);
            }

        }
    }
};


/*------------------------------------------------------------------------*/

//Extract list of links of GPX
async function GPX_link_generate() {
    for (district in lists) {
        for (link of lists[district]) {
            try {
                await GPX.getGPX_link(link);
            } catch (error) {
                console.log(error);
            }
        }
    }
};

//Read data extracted with gpx link
const gpx_links = JSON.parse(fs.readFileSync('./data/route_GPX/GPX_links.json'));

//Get all GPX data
async function GPX_data_download() {
    for (let i = 0; i < gpx_links.length; i++) {
        try {
            await download.download_file(gpx_links[i]['gpx_link'], `./data/route_GPX/${gpx_links[i]['name']}.gpx`);
            await fs.writeFile(`./data/route_GPX/GPX_download_log.json`, `${i}: GPX file downloaded: ${gpx_links[i]['name']}\n`, { flag: 'a' }, (err) => {
                if (err) throw err;
                console.log(`${i}: GPX file downloaded: ${gpx_links[i]['name']}`)
            });

        } catch (error) {
            console.log(error);
        }
    }
};

/*------------------------------------------------------------------------*/

//Extract list of links of Pic
async function Pic_link_generate() {
    for (district in lists) {
        for (link of lists[district]) {
            try {
                await pic.getPic_link(link);
            } catch (error) {
                console.log(error);
            }
        }
    }
};

//Read data extracted with pic link
const pic_links = JSON.parse(fs.readFileSync('./data/route_pic/pic_links.json'));


//Get images by links
async function Pic_download() {
    for (let i = 0; i < pic_links.length; i++) {
        try {
            await download.download_file(pic_links[i]['pic_link'], `./data/route_pic/${pic_links[i]['name']}.jpg`);
            await fs.writeFile(`./data/route_pic/pic_download_log.json`, `${i}: Route pic downloaded: ${pic_links[i]['name']}\n`, { flag: 'a' }, (err) => {
                if (err) throw err;
                console.log(`${i}: Route pic downloaded: ${pic_links[i]['name']}`)
            });

        } catch (error) {
            console.log(error);
        }
    }
};

/*Checking function as below----------------------------------------------------------*/
const routeinfo_log = JSON.parse(fs.readFileSync('./data/route_info/route_download_log_modified.json'));
const routeGPX_log = JSON.parse(fs.readFileSync('./data/route_GPX/GPX_download_log_modified.json'));


async function checking(log) {
    let name_link = {};
    for (link of pic_links) {
        if(!log.includes(link['name']))
        name_link[link['name']] = link['page_link']
    }
    console.log(name_link)
}

checking(routeinfo_log);
//checking(routeGPX_log);
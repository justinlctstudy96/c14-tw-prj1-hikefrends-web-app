// Import the playwright library into our scraper.
const playwright = require('playwright');


async function get_regions(path) {
    let links = {};
    // Open a Chromium browser. We use headless: false
    // to be able to watch what's going on.
    const browser = await playwright.chromium.launch({
        headless: true
    });
    // Open a new page / tab in the browser.
    const page = await browser.newPage();
    await page.setDefaultNavigationTimeout(0); 
    // Tell the tab to navigate to the JavaScript topic page.
    await page.goto(path);
    // Extract the required information from the site

    const result = await page.evaluate(() => {
        let link_list = [];
        const links = document.querySelectorAll('a[href^="http://follo3me.com"]');
        for (link of links) {
            link_list.push(link.getAttribute('href'));
        }
        return link_list.splice(2, 8)

    });
    // Pause for 10 seconds, to see what's going on.
    //await page.waitForTimeout(10000);

    // Turn off the browser to clean up after ourselves.

    await browser.close();

    //Data cleaning
    const link_title = ['香港島', '九龍.將軍澳', '西貢', '大嶼山', '新界東北', '新界西北', '新界中部', '離島.其他島嶼'];
    for (let i = 0; i < link_title.length; i++) {
        let split_result = result[i].split(":");
        let concat_result = split_result[0].concat("s:",split_result[1]);
        links[link_title[i]] = concat_result;
    }
    return links
}
async function checking() {
    console.log(await get_regions('https://follo3me.com/'))
}

exports.get_regions = get_regions;
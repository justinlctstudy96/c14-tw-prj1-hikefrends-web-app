const { chromium } = require('playwright');

(async () => {
  const browser = await chromium.launch({
    headless: false
  });
  const context = await browser.newContext();

  // Open new page
  const page = await context.newPage();

  // Go to https://follo3me.com/kowloon-tko/chiu-keng-wan-shan/
  await page.goto('https://follo3me.com/kowloon-tko/chiu-keng-wan-shan/');

  // Click text=下載路線
  const [download] = await Promise.all([
    page.waitForEvent('download'),
    page.click('text=下載路線')
  ]);

  // Close page
  await page.close();

  // ---------------------
  await context.close();
  await browser.close();
})();
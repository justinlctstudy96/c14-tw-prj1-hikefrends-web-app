// Import the required modules.
const fs = require('fs');
const fetch = require('node-fetch')

//create function to download GPX files
async function download_file(url, path_filename) {
    const res = await fetch(url);
    const fileStream = fs.createWriteStream(path_filename);
    await new Promise((resolve, reject) => {
        res.body.pipe(fileStream);
        res.body.on("error", reject);
        fileStream.on("finish", resolve);
    });

};


exports.download_file = download_file;

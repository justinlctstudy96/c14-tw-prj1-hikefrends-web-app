import { Client } from 'pg';
import dotenv from 'dotenv';
// import {main} from 'scraper_followme.js'
// import { split } from 'ts-node';
// import xlsx from 'xlsx';
//  用dotenv load 咗.env 入面嘅內容入嚟
dotenv.config();

// process.env 令你可以access process 嘅environment
export const client = new Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
})


export interface Route {
    name: string
    type: string
    region: string
    district: string
    difficulty: number
    length: number
    elevation_gain: number
    expected_time: number
    description: string
}

export interface coordinate {
    latitude: number
    longitude: number
    elevation: number
}


// export async function insertJsonb(json:JSON) {
//     await client.connect();
//     await client.query(`insert into routes (coordinates,created_at,updated_at) values ($7,NOW(),NOW())`,[memo.content,memo.image]);

// }

// main();

export async function csvToJsonb(filePath: string) {
    // await client.connect()

    var fs = require("fs");

    var data = fs.readFileSync(filePath);
    var stringData = data.toString();
    // console.log(stringData);
    var arrayOne = stringData.split('\r\n');

    var header = arrayOne[0].split(',');
    var noOfRow = arrayOne.length;
    var noOfCol = header.length;

    var jArray = [];

    var i = 0, j = 0;
    for (i = 1; i < noOfRow - 1; i++) {

        var obj = {};
        var myNewLine = arrayOne[i].split(',');

        for (j = 0; j < noOfCol; j++) {
            var headerText = header[j].substring(1, header[j].length - 1);
            var valueText = myNewLine[j].substring(1, myNewLine[j].length - 1);
            obj[headerText] = valueText;
        };
        jArray.push(obj);
    };
    // console.log(jArray);
}

// csvToJsonb('./routeCSV/麥理浩徑第3段 西-东.csv');


export async function gpxToJsonb(filePath: string) {
    var fs = require("fs");
    var data = fs.readFileSync(filePath);
    var stringData = data.toString();
    var trkpts = stringData.split('<trkpt')
    var jsonbCoordinates = {}
    for (var i = 1; i < trkpts.length; i++) {
        var obj = {}
        let splits = trkpts[i].split(/ |><ele>/)
        let latitude = splits[1].substring(5, 15).replace('"', '')
        let longitude = splits[2].substring(5, 15).replace('"', '')
        let z = splits[3].indexOf('<')
        let elevation = splits[3].substring(0, z)
        obj['latitude'] = latitude
        obj['longitude'] = longitude
        obj['elevation'] = elevation
        // jArray.push(obj)
        jsonbCoordinates[i] = obj
    }
    // console.log(jArray)

    return jsonbCoordinates;
}

// gpxToJsonb('./scraper/data/).gpx');

// export async function jsonTolnglat() {
//     let coordinate = []
//     var jsonCoords: any[] = await gpxToJsonb('./routeGPX/斜炮頂(犀牛石).gpx')
//     var lnglat = []
//     for (let jsonCoord of jsonCoords) {
//         coordinate = []
//         coordinate[0] = Number(jsonCoord.longitude)
//         coordinate[1] = Number(jsonCoord.latitude)
//         lnglat.push(coordinate)

//     }
//     // console.log(lnglat)
// }

// jsonTolnglat()

export async function jsonToSQL(jsonPath: string) {
    // await client.connect()
    var fs = require("fs");
    try {
        var json = JSON.parse(fs.readFileSync(jsonPath))
        // console.log(json)
        await client.query(`insert into routes (name, description, notes, difficulty, length, dimension, route, transport, created_at, updated_at) 
    values ($1,$2,$3,$4,$5,$6,$7,$8,NOW(),NOW())`, [json.name, json.description, json.others, json.difficulty, json.length, json.dimension, json.route, json.transport])
        // await client.query(`insert into routes (transport,created_at,updated_at) values($1,NOW(),NOW())`,[json.transport])
        // console.log('sql')
    } catch (err) {
        console.log(`No ${jsonPath}`)
    }
}

// jsonToSQL('./scraper/data/鶴咀山 D’ Aguilar Peak.json')


export async function allJsonToSQL(jsonInfoPath: string, routeJsonb: {}) {
    // await client.connect()
    var fs = require("fs");
    try {
        var info = JSON.parse(fs.readFileSync(jsonInfoPath))
        // var routeCoordinates = await gpxToJsonb(GpxPath)
        await client.query(`insert into routes (name, district,description, notes, difficulty, length, dimension, route, transport, coordinates, created_at, updated_at) 
        values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,NOW(),NOW())`, [info.name, info.district, info.description, info.others, info.difficulty, info.length, info.dimension, info.route, info.transport, routeJsonb])
    } catch (err) { console.log(`No ${jsonInfoPath}`) }
}

let routeCoordinates = {}
export async function specificInputSQL() {
    await client.connect()
    routeCoordinates = await gpxToJsonb('./scraper/scraper_specific/相思林徑.gpx')
    allJsonToSQL('./scraper/scraper_specific/相思林徑 Sheung Sze Forest Trail.json', routeCoordinates)
    routeCoordinates = await gpxToJsonb('./scraper/scraper_specific/香港仔水塘.gpx')
    allJsonToSQL('./scraper/scraper_specific/香港仔水塘 Aberdeen Reservoirs.json', routeCoordinates)
    routeCoordinates = await gpxToJsonb('./scraper/scraper_specific/鳳冠南巖.gpx')
    allJsonToSQL('./scraper/scraper_specific/鳳冠南巖 South Phoenix Trail.json', routeCoordinates)
}

// specificInputSQL()

//  console.log(gpxToJsonb('./scraper/scraper_specific/相思林徑.gpx'))

// , infoFolder:string, gpxFolder:string
export async function allRouteInfoToSQL(routeNameList: string, gpxFolder: string, infoFolder: string) {
    await client.connect()
    var fs = require("fs")
    var routeNameList: string = JSON.parse(fs.readFileSync(routeNameList))
    console.log(routeNameList)
    for (let routeName of routeNameList) {
        try {
            let routeCoordinates = await gpxToJsonb(`${gpxFolder}/${routeName}.gpx`)
            allJsonToSQL(`${infoFolder}/${routeName}.json`, routeCoordinates)
        } catch (err) { }
    }
}

allRouteInfoToSQL('./scraper/data/route_GPX/GPX_download_log_modified.json', './scraper/data/route_GPX', './scraper/data/route_info')

select *
from threads
    join routes on routes.id = threads.route_id;
select threads.id
from threads
    join routes on routes.id = threads.route_id;
select threads.id,
    routes.district,
    threads.title,
    threads.upvote,
    threads.created_at
FROM threads
    left join routes on routes.id = threads.route_id;
select *
from threads
    left join users on threads.host_id = users.id
where threads.id = 2;
insert into comments (user_id, thread_id, content, upvote, created_at)
VALUES (5, 2, 'this is the first comment haha', 3, NOW())
insert into comments (user_id, thread_id, content, upvote, created_at)
VALUES (5, 2, 'this is the second com', 10, NOW())
select *
from comments;
select name
from routes
where district = '香港島';
select threads.id,
    routes.district,
    routes.name,
    threads.title,
    threads.upvote,
    threads.created_at
from threads
    left join routes on routes.id = threads.route_id
ORDER BY threads.created_at DESC;
select threads.id,
    routes.district,
    routes.name,
    threads.title,
    threads.upvote,
    threads.created_at,
    max(comments.updated_at) AS max_updated_at
from threads
    left join routes on routes.id = threads.route_id
    left join comments on threads.id = comments.thread_id
GROUP BY threads.id,
    comments.thread_id;
SELECT threads.id,
    routes.district,
    routes.name,
    threads.title,
    threads.upvote,
    threads.created_at,
    sorted_comments.max_updated_at
FROM threads
    LEFT JOIN routes on threads.route_id = routes.id
ORDER BY threads.created_at DESC
    LEFT JOIN (
        SELECT comments.thread_id AS thread_id,
            max(comments.updated_at) AS max_updated_at
        FROM comments
        GROUP BY comments.thread_id
    ) AS sorted_comments on threads.id = sorted_comments.thread_id
ORDER BY max_updated_at DESC;
SELECT *
FROM threads;
SELECT *
FROM routes;
SELECT *
FROM comments;
SELECT threads.id,
    routes.district,
    routes.name,
    threads.title,
    threads.upvote,
    threads.created_at,
    sorted_comments.max_updated_at
FROM threads
    LEFT JOIN routes on threads.route_id = routes.id
    LEFT JOIN (
        SELECT comments.thread_id AS thread_id,
            max(comments.updated_at) AS max_updated_at
        FROM comments
        GROUP BY comments.thread_id
    ) AS sorted_comments on threads.id = sorted_comments.thread_id
where routes.district = '香港島'
ORDER BY max_updated_at DESC;
CREATE TABLE guild_chat(
    id SERIAL primary key,
    guild_id integer,
    user_id INTEGER,
    FOREIGN KEY (user_id) REFERENCES users(id),
    content text,
    created_at timestamp
);
ALTER guild_user
ADD COLUMN last_online timestamp
select *
from guild_chat
    left join users on guild_chat.user_id = users.id
where guild_chat.guild_id = 1
select *
from activities;
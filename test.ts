global.fetch = require("node-fetch");

async function getDirection() {
    var res = await fetch('https://api.mapbox.com/directions/v5/mapbox/cycling/-84.518641,39.134270;-84.512023,39.102779?geometries=geojson&access_token=pk.eyJ1IjoianMxMzk0MzciLCJhIjoiY2ttcnh0NGsyMGNkYTJvb3liYjU1a2V3cyJ9.BXthxGVcKZtAudZFTfovWA')
    var direction = await res.json();
    // const notification = document.querySelector('#head_info');
    console.log(JSON.stringify(direction))
}
getDirection()
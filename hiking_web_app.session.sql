CREATE TABLE guilds(
    id SERIAL PRIMARY KEY,
    name TEXT,
    description TEXT,
    level INTEGER,
    experience DECIMAL(5, 2),
    icon varchar(255),
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);
CREATE TABLE users(
    id SERIAL primary key,
    email varchar(255),
    password varchar(255),
    nickname varchar(255),
    experience decimal(10, 2),
    clover integer,
    profilepic varchar(255),
    last_online varchar(255),
    guild_id int
);

/* ALTER TABLE users
DROP column last_login_time

ALTER TABLE users
ADD last_online varchar(255); */

CREATE TABLE guild_user(
    id SERIAL PRIMARY KEY,
    guild_id integer,
    FOREIGN KEY (guild_id) REFERENCES guilds(id),
    user_id integer,
    FOREIGN KEY (user_id) REFERENCES users(id),
    contribution integer,
    position varchar(255),
    joined_at TIMESTAMP    
);

/* ALTER TABLE guild_user
DROP column last_online */

CREATE TABLE guild_user_request(
    id SERIAL PRIMARY KEY,
    guild_id integer,
    FOREIGN KEY (guild_id) REFERENCES guilds(id),
    user_id integer,
    FOREIGN KEY (user_id) REFERENCES users(id),
    request_at TIMESTAMP
);
CREATE TABLE routes(
    id SERIAL primary key,
    name TEXT,
    district TEXT,
    description TEXT,
    notes jsonb,
    difficulty DECIMAL(5, 2),
    length DECIMAL(5, 2),
    dimension jsonb,
    route jsonb,
    transport jsonb,
    coordinates jsonb,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);
CREATE TABLE activities(
    id SERIAL primary key,
    guild_id integer,
    public bit,
    route_id integer,
    team_size integer,
    leader_id integer,
    FOREIGN KEY(leader_id) REFERENCES users(id),
    participant_id jsonb,
    -- FOREIGN KEY(participant) REFERENCES users(id),
    description varchar(255),
    date varchar(255),
    status text,
    center jsonb,
    start_time timestamp with time zone,
    end_time timestamp with time zone,
    hiked_distance decimal(10, 3),
    -- chatroom_id int,
    -- FOREIGN KEY(chatroom_id) REFERENCES chatrooms(id),
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);
CREATE TABLE participants_activities(
    id SERIAL primary key,
    user_id integer,
    FOREIGN KEY (user_id) REFERENCES users(id),
    activity_id integer,
    FOREIGN KEY (activity_id) REFERENCES activities(id),
    created_at timestamp with time zone
);
CREATE TABLE requests_activity(
    id SERIAL primary key,
    user_id integer,
    FOREIGN KEY (user_id) REFERENCES users(id),
    activity_id integer,
    FOREIGN KEY (activity_id) REFERENCES activities(id),
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);

CREATE TABLE threads(
    id SERIAL primary key,
    host_id INTEGER,
    FOREIGN KEY (host_id) REFERENCES users(id),
    route_id integer,
    FOREIGN KEY (route_id) REFERENCES routes(id),
    title varchar(255),
    content text,
    upvote integer,
    created_at timestamp,
    image text
) 


CREATE TABLE comments(
    id SERIAL primary key,
    user_id integer,
    FOREIGN KEY (user_id) REFERENCES users(id),
    thread_id integer,
    FOREIGN KEY (thread_id) REFERENCES threads(id),
    content text,
    upvote integer,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    image text
) 

CREATE TABLE guild_chat (
    id SERIAL primary key,
    guild_id integer,
    user_id INTEGER,
    FOREIGN KEY (user_id) REFERENCES users(id),
    content text,
    created_at timestamp
);

CREATE TABLE activity_chat (
    id SERIAL primary key,
    activity_id integer,
    user_id INTEGER,
    FOREIGN KEY (user_id) REFERENCES users(id),
    content text,
    created_at timestamp
);